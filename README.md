# DemoNeo4jAuditing using Spring Data


This project shows how to auditing NodeEntities on Neo4j in two different ways, solving the problem of auditing using a different Long ID strategy

1. using annotations
2. using event listeners

by default event listeners are commented



For running a Neo4j docker container just run

```sh
docker run \                                                                                                              
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    neo4j
```

to run the proyect 
```
./gradlew bootRun
```

