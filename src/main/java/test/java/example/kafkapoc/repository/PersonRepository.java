package test.java.example.kafkapoc.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;
import test.java.example.kafkapoc.domain.Person;

import java.util.UUID;

/**
 * PersonRepository
 *
 * @author christian.valencia
 * @since 8/07/20
 */
@Repository
public interface PersonRepository extends Neo4jRepository<Person, String> {

    Person findByName(String name);
}
