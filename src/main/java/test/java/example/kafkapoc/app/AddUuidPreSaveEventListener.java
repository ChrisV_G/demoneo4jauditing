package test.java.example.kafkapoc.app;

import org.neo4j.ogm.session.event.Event;
import org.neo4j.ogm.session.event.EventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import test.java.example.kafkapoc.domain.Person;

/**
 * EventListener
 *
 * @author christian.valencia
 * @since 8/07/20
 */
@Component
public class AddUuidPreSaveEventListener implements EventListener {

    @Autowired
    private AuditorAware auditorAware;

    //second way to do the auditing
    public void onPreSave(Event event) {
        Person entity = (Person) event.getObject();
        if (entity.getCreatedDate() == null) {
            System.out.println("Creating New Objet");
//            entity.setId(UUID.randomUUID().toString());
//            entity.setCreatedDate(LocalDateTime.now());
//            entity.setCreatedBy(auditorAware.getCurrentAuditor().get().toString());
        } else {
            System.out.println("Editing object");
//            entity.setLastUpdatedBy(auditorAware.getCurrentAuditor().get().toString());
//            entity.setLastUpdatedDate(LocalDateTime.now());
        }
    }

    public void onPostSave(Event event) {
    }

    public void onPreDelete(Event event) {
    }

    public void onPostDelete(Event event) {
    }

}