package test.java.example.kafkapoc.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.neo4j.annotation.EnableNeo4jAuditing;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import test.java.example.kafkapoc.domain.Person;
import test.java.example.kafkapoc.repository.PersonRepository;


@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"test.java.example.kafkapoc"})
@EnableCaching
@EnableNeo4jRepositories(basePackages = "test.java.example.kafkapoc.repository")
@EnableNeo4jAuditing(auditorAwareRef = "auditorProvider")
@EntityScan(basePackages = "test.java.example.kafkapoc.domain")
public class Neo4jAuditingApplication {

    public static void main(String[] args) {
        SpringApplication.run(Neo4jAuditingApplication.class, args);
    }

    @Bean
    CommandLineRunner demo(PersonRepository personRepository) {
        return args -> {
            create(personRepository);
            update(personRepository);
        };
    }

    private void create(PersonRepository personRepository) {
        personRepository.deleteAll();
        Person wilson = new Person("Wilson");
        personRepository.save(wilson);
        Person wilsonQueried = personRepository.findByName(wilson.getName());

        System.out.println("Name: " + wilsonQueried.getName() + " Created Date: " + wilsonQueried.getCreatedDate());
    }

    private void update(PersonRepository personRepository) {
        Person carlos = personRepository.findByName("Wilson");
        carlos.setName("Carlos");
        personRepository.save(carlos);
        System.out.println("Name: " + carlos.getName() + " Created Date: " + carlos.getCreatedDate());
    }

    @Bean("auditorProvider")
    public AuditorAware<String> auditorProvider() {
        return new AuditorAwareImpl();
    }
}

