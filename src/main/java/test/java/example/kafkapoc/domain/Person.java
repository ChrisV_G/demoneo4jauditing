package test.java.example.kafkapoc.domain;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.id.UuidStrategy;

/**
 * Person
 *
 * @author christian.valencia
 * @since 8/07/20
 */
@NodeEntity
public class Person extends Auditable{

    @Id
    @GeneratedValue(strategy = UuidStrategy.class)
    private String id;

    private String name;


    public Person(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}